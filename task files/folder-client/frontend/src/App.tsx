import React, { useState } from 'react';
import type { IFolder, IMergedFolder, IModalState } from "../types/types";
import './App.css';
import { Folder } from './components/Folder';
import { useFetch } from './hooks/useFetch';
import Modal from './shared/Modal';
import { mergeFolders } from './utils/mergeFolders';

interface IContext {
  folders: IFolder[]
  mergedFolders: IMergedFolder[],
  setFolders: React.Dispatch<React.SetStateAction<IFolder[]>>
  modalState: IModalState,
  setModalState: React.Dispatch<React.SetStateAction<IModalState>>
}

export const AppContext = React.createContext<IContext>({} as any);

function App() {
  const [folders, setFolders] = useState([] as IFolder[]);
  const [modalState, setModalState] = useState<IModalState>({
    children: null,
    open: false
  });
  const response = useFetch<{ folders: IFolder[] }>('http://localhost:3000/api/v1/folders/', (data) => {
    setFolders(data.folders)
  });

  const mergedFolders = mergeFolders(folders);

  return (
    <AppContext.Provider value={{ folders, mergedFolders, setFolders, modalState, setModalState }}>
      <Modal modalState={modalState} setModalState={setModalState} />
      {response.error ? <div>{response.error}</div> : response.loading ? <div>Fetching data</div> :
        <div className="App">
          {mergedFolders.map(mergedFolder => <Folder depth={0} folder={mergedFolder} key={mergedFolder._id} />)}
        </div>
      }
    </AppContext.Provider>
  );
}

export default App;
