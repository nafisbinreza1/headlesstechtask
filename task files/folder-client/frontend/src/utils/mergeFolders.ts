import type { IFolder, IMergedFolder } from "../../types/types";

export function mergeFolders(folders: IFolder[]){
  const mergedFolders: Record<string, IMergedFolder> = {};
  folders.forEach(folder=>{
    mergedFolders[folder._id] = {
      ...folder,
      children: []
    }
  });

  folders.forEach(folder=> {
    const parentFolder = mergedFolders[folder.parent];
    const mergedFolder = mergedFolders[folder._id];
    if(parentFolder)
      parentFolder.children.push(mergedFolder)
  });

  return Object.values(mergedFolders).filter(mergedFolder=>mergedFolder.parent === undefined);
}