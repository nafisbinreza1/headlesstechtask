export async function mutateData<D, R>(method: 'POST' | 'PUT' | 'PATCH' | 'DELETE', url: string, data?: D): Promise<R> {
  const response = await fetch(url, {
    method,
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data ?? {})
  });
  return response.json();
}