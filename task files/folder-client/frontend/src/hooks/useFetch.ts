import { useEffect, useState } from "react";
import type { TResponse } from "types/types";
interface IState<T>{
  data: T | null,
  error: string | null,
  loading: boolean
}

export function useFetch<T>(url: string, setData: (data: T)=>any){
  const [state, setState] = useState<IState<T>>({
    data: null,
    error: null,
    loading: true
  });

  useEffect(()=> {
    fetch(url).then(response=> response.json() as Promise<TResponse<T>>).then(response=> {
      if(response.status==='success'){
        setData(response.data)
        setState({
          data: response.data,
          error: null,
          loading: false
        })
      }
    }).catch(err=>{
      setState({
        data: null,
        error: err.message,
        loading: false
      })
    })
  }, [])
  
  return state;
}