import React, { useContext, useState } from "react";
import { Button } from "../../../../src/shared/Button";
import { Heading } from "../../../../src/shared/Heading";
import type { IFolder, IFolderInput, TResponse } from "../../../../types/types";
import { AppContext } from "../../../App";
import { mutateData } from "../../../utils/mutateData";
import styles from "./style.module.css";
interface Props {
  folder: IFolder
}

export function CreateFolderModal(props: Props) {
  const { folders, setFolders, setModalState } = useContext(AppContext);
  const [folderName, setFolderName] = useState('');

  return <div className={styles.container}>
    <Heading label={`Add folder in ${props.folder.name}`} />
    <input style={{ borderColor: folderName === '' ? '#ff2828' : '#676767' }} className={styles.input} ref={(inputRef) => {
      inputRef?.focus()
    }} onChange={(e) => setFolderName(e.target.value)} value={folderName} />
    <div className={styles.buttons}>
      <Button state={'neutral'} onClick={async () => {
        if (folderName !== '') {
          setModalState({
            children: null,
            open: false
          })
          const response = await mutateData<IFolderInput, TResponse<{ folder: IFolder }>>('POST', `http://localhost:3000/api/v1/folders/`, {
            name: folderName,
            parent: props.folder._id
          });
          if (response.status === 'success')
            setFolders([...folders, response.data.folder])
        }
      }} label={"Create"} />
      <Button state={'danger'} onClick={() => {
        setModalState({
          children: null,
          open: false
        })
      }} label={"Cancel"} />
    </div>
  </div>
}