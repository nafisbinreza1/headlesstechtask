import React, { useContext } from "react";
import type { IMergedFolder } from "types/types";
import { AppContext } from "../../App";
import { CreateFolderModal } from "./CreateFolderModal";
import { DeleteFolderModal } from "./DeleteFolderModal";
import "./style.css";

interface Props {
  folder: IMergedFolder
  depth: number
}

export function Folder(props: Props) {
  const { setModalState } = useContext(AppContext);

  return <div className="folder" style={{ marginLeft: 25 * props.depth }}>
    <div className="folder-parent">
      <div className="folder-name">{props.folder.name}</div>
      <div className="folder-new icon" onClick={() => {
        setModalState({
          children: <CreateFolderModal folder={props.folder} />,
          open: true
        })
      }}>+</div>
      <div className="folder-delete icon" onClick={
        () => {
          setModalState({
            children: <DeleteFolderModal folder={props.folder} />,
            open: true
          })
        }}>-</div>
    </div>
    <div className="folder-children">
      {props.folder.children.length === 0 ? <div>No folders</div> : props.folder.children.map(folder => <Folder depth={props.depth + 1} key={folder._id} folder={folder} />)}
    </div>
  </div>
}