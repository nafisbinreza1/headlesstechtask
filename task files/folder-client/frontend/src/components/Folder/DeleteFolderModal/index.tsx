import React, { useContext } from "react";
import { Button } from "../../../../src/shared/Button";
import { Heading } from "../../../../src/shared/Heading";
import type { IFolder, IFolderInput, TResponse } from "../../../../types/types";
import { AppContext } from "../../../App";
import { mutateData } from "../../../utils/mutateData";
import styles from "./style.module.css";

interface Props {
  folder: IFolder
}

export function DeleteFolderModal(props: Props) {
  const { folders, setFolders, setModalState } = useContext(AppContext);

  return <div className={styles.container}>
    <Heading label={`Delete ${props.folder.name}`} />
    <div className={styles.buttons}>
      <Button state={'danger'} onClick={async () => {
        setModalState({
          children: null,
          open: false
        })
        const response = await mutateData<IFolderInput, TResponse<{ folder: IFolder }>>('DELETE', `http://localhost:3000/api/v1/folders/${props.folder._id}`);
        if (response.status === 'success')
          setFolders(folders.filter(folder => folder._id !== props.folder._id))
      }} label={"Delete"} />
      <Button state={'neutral'} onClick={() => {
        setModalState({
          children: null,
          open: false
        })
      }} label={"Cancel"} />
    </div>
  </div>
}