import React from "react";
import type { IModalState } from "../../../types/types";
import './style.css';

interface Props {
  modalState: IModalState
  setModalState: React.Dispatch<React.SetStateAction<IModalState>>
}

const Modal = ({ modalState }: Props) => {
  const showHideClassName = modalState.open ? "modal display-block" : "modal display-none";

  return (
    <div className={showHideClassName}>
      <section className="modal-main">
        {modalState.children}
      </section>
    </div>
  );
};

export default Modal;