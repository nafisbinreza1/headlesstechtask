import React from "react";
import "./style.css";

interface Props {
  label: string
  onClick: () => void
  state: 'danger' | 'neutral'
}

export function Button(props: Props) {
  let backgroundColor: string = 'black'
  if (props.state === 'danger')
    backgroundColor = '#ff2828'
  else if (props.state === 'neutral')
    backgroundColor = '#3f51b5'
  return <button className="button" style={{ backgroundColor }} onClick={props.onClick}>{props.label}</button>
}