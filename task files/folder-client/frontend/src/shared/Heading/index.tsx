import React from "react";
import "./style.css";

interface Props {
  label: string
}

export function Heading(props: Props) {
  return <div className="heading">{props.label}</div>
}