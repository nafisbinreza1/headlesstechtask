export interface IFolderInput {
  name: string
  parent: string
}

export interface IFolder extends IFolderInput{
  _id: string
}

export interface IMergedFolder extends IFolder{
  children: IMergedFolder[]
}

export interface ISuccessResponse<T>{
  status: "success",
  data: T
}

export interface IFailedResponse{
  status: 'failure',
  message: string
}

export interface IModalState{
  children: null | JSX.Element,
  open: boolean
}

export type TResponse<T> = ISuccessResponse<T> | IFailedResponse;