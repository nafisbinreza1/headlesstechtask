import cors from 'cors';
import express from 'express';
import mongoose from 'mongoose';
import { MONGO_HOST, MONGO_PASSWORD, MONGO_USER, NODE_ENV, PORT } from './configs';
import FolderRouter from './routes/folder';

const app = express();

app.enable('trust proxy');
app.use(cors());

function retryDatabaseConnection() {
  console.log('Retrying mongodb connection');
  mongoose
    .connect(
      `mongodb+srv://${MONGO_USER}:${MONGO_PASSWORD}@${MONGO_HOST}/folder?authSource=admin`,
      {
        useUnifiedTopology: true,
        useNewUrlParser: true,
        useFindAndModify: false,
        useCreateIndex: true
      }
    )
    .then(() => {
      console.log('Successfully connected to database');
    })
    .catch((err) => {
      console.log('Error', err.message);
      setTimeout(retryDatabaseConnection, 5000);
    });
}

retryDatabaseConnection();

app.get('/ping', (_, res) => {
  res.send(`<h2>Listening at port ${PORT} in ${NODE_ENV} environment</h2>`);
});

app.use(express.json());
app.use('/api/v1/folders', FolderRouter);
app.listen(PORT, () =>
  console.log(`Listening at port ${PORT} in ${NODE_ENV} environment`)
);