import express from 'express';
import { createFolder, deleteFolder, getAllFolders, updateFolder } from '../controllers/folder';

const router = express.Router();

router
  .route('/')
  .get(getAllFolders)
  .post(createFolder)

router
  .route('/:id')
  .patch(updateFolder)
  .delete(deleteFolder)

export default router