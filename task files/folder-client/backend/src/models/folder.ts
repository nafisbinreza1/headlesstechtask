import { Document, model, Model, Schema } from 'mongoose';

interface IFolder extends Document{
  name: string
  parent: string
}

const FolderSchema = new Schema({
  name: {
    type: String,
    required: [true, 'Folder must have a name']
  },
  parent: { type: Schema.Types.ObjectId, ref: 'Folder' }
});

const FolderModel: Model<IFolder> = model('Folder', FolderSchema);
export default FolderModel;