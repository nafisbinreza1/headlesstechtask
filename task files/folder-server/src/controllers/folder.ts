import { Request, Response } from 'express';
import Folder from '../models/folder';

export const getAllFolders = async (_: Request, res: Response) => {
  try {
    const folders = await Folder.find();
    res.status(200).json({
      status: 'success',
      data: {
        folders
      }
    });
  } catch (err) {
    res.status(400).json({
      status: 'failure',
      message: err.message
    });
  }
};

export const createFolder = async (req: Request, res: Response) => {
  try {
    const folder = await Folder.create(req.body);
    res.status(200).json({
      status: 'success',
      data: {
        folder
      }
    });
  } catch (err) {
    res.status(400).json({
      status: 'failure',
      message: err.message
    });
  }
}

export const updateFolder = async (req: Request, res: Response) => {
  try {
    const folder = await Folder.findByIdAndUpdate(req.params.id, req.body, {
      new: true,
      runValidators: true
    });
    res.status(200).json({
      status: 'success',
      data: {
        folder
      }
    });
  } catch (err) {
    res.status(400).json({
      status: 'failure',
      message: err.message
    });
  }
};

export const deleteFolder = async (req: Request, res: Response) => {
  try {
    await Folder.findByIdAndDelete(req.params.id);
    await Folder.deleteMany({
      parent: req.params.id
    })
    res.status(200).json({
      status: 'success',
      data: {
        deleted: true
      }
    });
  } catch (err) {
    res.status(400).json({
      status: 'failure',
      message: err.message
    });
  }
};